package com.panha.springhomework.service;

import com.panha.springhomework.Pagination;
import com.panha.springhomework.repository.dto.BookDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {

    List<BookDto> getAllBooks(@Param("pagination")Pagination pagination);
    List<BookDto> getBookByTitle(String title);
    List<BookDto> getBooksByCategoryId(int id);
    List<BookDto> getBookByCategoryIdAndTitle(int id, String title);
    BookDto getBookById(int id);
    BookDto postBook(BookDto newBook);
    BookDto updateBook(BookDto updateBook,int id);
    String deleteBook(int id);
    int countAllBook();
}
