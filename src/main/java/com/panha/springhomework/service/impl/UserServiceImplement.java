package com.panha.springhomework.service.impl;

import com.panha.springhomework.repository.UserRepository;
import com.panha.springhomework.repository.dto.UserDto;
import com.panha.springhomework.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImplement implements UserService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("loadUserByUserName");
        System.out.println("User Detail = " + userRepository.selectUserByUsername(username));
        return userRepository.selectUserByUsername(username);
    }
}
