package com.panha.springhomework.service;

import com.panha.springhomework.repository.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface UserService extends UserDetailsService {


}
