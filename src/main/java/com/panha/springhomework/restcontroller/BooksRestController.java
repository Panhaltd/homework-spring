package com.panha.springhomework.restcontroller;

import com.panha.springhomework.Pagination;
import com.panha.springhomework.repository.dto.BookDto;
import com.panha.springhomework.restcontroller.request.BookRequestModel;
import com.panha.springhomework.restcontroller.response.BaseApiResponse;
import com.panha.springhomework.service.impl.BookServiceImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.panha.springhomework.utitl.MethodContainer;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;

@RestController
public class BooksRestController {

    private BookServiceImplement bookServiceImplement;
    MethodContainer methods;

    @Autowired
    public void setMethods(MethodContainer methods) {
        this.methods = methods;
    }

    @Autowired
    public void setBookServiceImplement(BookServiceImplement bookServiceImplement) {
        this.bookServiceImplement = bookServiceImplement;
    }

    //Get all books
    @GetMapping("/books")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> getAllBooks(
            @RequestParam(value = "page",required = false,defaultValue = "1")int page,
            @RequestParam(value = "limit",required = false,defaultValue = "3")int limit
    ){
        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();
        Pagination pagination=new Pagination(page,limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.setTotalCount(bookServiceImplement.countAllBook());
        pagination.setTotalPages(pagination.getTotalPages());
        List<BookDto> bookDtoList = bookServiceImplement.getAllBooks(pagination);
        List<BookRequestModel> books =new ArrayList<>();

        for (BookDto book : bookDtoList){
            books.add(methods.getMapper().map(book,BookRequestModel.class));
        }
        response.setPagination(pagination);
        response.setMessage("You have got all data successfully!");
        response.setData(books);
        response.setStatus(HttpStatus.OK);
        response.setTime(methods.getTime());

        return ResponseEntity.ok(response);
    }

    //Get book by id
    @GetMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> getBookById(@PathVariable int id){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        BookDto bookDto = bookServiceImplement.getBookById(id);
        BookRequestModel book = methods.getMapper().map(bookDto,BookRequestModel.class);

        response.setMessage("You have found book!");
        response.setData(book);
        response.setStatus(HttpStatus.OK);
        response.setTime(methods.getTime());

        return ResponseEntity.ok(response);
    }

    //Post a book
    @PostMapping("/books")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> postBook(@RequestBody BookRequestModel newBook){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        BookDto book = methods.getMapper().map(newBook,BookDto.class);
        BookDto bookDto = bookServiceImplement.postBook(book);

        BookRequestModel bookRequest = methods.getMapper().map(bookDto,BookRequestModel.class);

        response.setMessage("You have posted a new book!");
        response.setData(bookRequest);
        response.setStatus(HttpStatus.OK);
        response.setTime(methods.getTime());

        return ResponseEntity.ok(response);
    }

    //Update a book
    @PutMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> updateBook(@PathVariable int id,@RequestBody BookRequestModel newBook){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        BookDto book = methods.getMapper().map(newBook,BookDto.class);
        BookDto bookDto = bookServiceImplement.updateBook(book,id);

        BookRequestModel bookRequest = methods.getMapper().map(bookDto,BookRequestModel.class);

        response.setMessage("You have updated a book!");
        response.setData(bookRequest);
        response.setStatus(HttpStatus.OK);
        response.setTime(methods.getTime());

        return ResponseEntity.ok(response);
    }

    //Delete a Book
    @DeleteMapping("/books/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable int id){

        String message = bookServiceImplement.deleteBook(id);

        return ResponseEntity.ok(message);
    }

    //Get Book by filtering category
    @RequestMapping(value = "/books",method = RequestMethod.GET,params = {"categoryId"})
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> getBookByFilterCategory(@RequestParam("categoryId") int id){

        System.out.println(id);
        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();

        List<BookDto> bookDtoList;
            bookDtoList = bookServiceImplement.getBooksByCategoryId(id);
        List<BookRequestModel> books =new ArrayList<>();

        for (BookDto book : bookDtoList){
            books.add(methods.getMapper().map(book,BookRequestModel.class));
        }

        response.setMessage("You have got all data successfully!");
        response.setData(books);
        response.setStatus(HttpStatus.OK);
        response.setTime(methods.getTime());

        return ResponseEntity.ok(response);
    }


    //Get Book By Title
    @RequestMapping(value = "/books",method = RequestMethod.GET,params = {"title"})
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> getBookByTitle(@RequestParam("title") String title){

        System.out.println(" "+title);
        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();

        List<BookDto> bookDtoList;
        bookDtoList = bookServiceImplement.getBookByTitle(title);
        List<BookRequestModel> books =new ArrayList<>();

        for (BookDto book : bookDtoList){
            books.add(methods.getMapper().map(book,BookRequestModel.class));
        }

        response.setMessage("You have got all data successfully!");
        response.setData(books);
        response.setStatus(HttpStatus.OK);
        response.setTime(methods.getTime());

        return ResponseEntity.ok(response);
    }

    //Get Book by Category and Title
    @RequestMapping(value = "/books",method = RequestMethod.GET,params = {"categoryId","title"})
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> getBookByCategoryIdAndTitle(@RequestParam("categoryId") int id, @RequestParam("title") String title){

        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();

        List<BookDto> bookDtoList;
        bookDtoList = bookServiceImplement.getBookByCategoryIdAndTitle(id,title);
        List<BookRequestModel> books =new ArrayList<>();

        for (BookDto book : bookDtoList){
            books.add(methods.getMapper().map(book,BookRequestModel.class));
        }

        response.setMessage("You have got all data successfully!");
        response.setData(books);
        response.setStatus(HttpStatus.OK);
        response.setTime(methods.getTime());

        return ResponseEntity.ok(response);
    }

}
