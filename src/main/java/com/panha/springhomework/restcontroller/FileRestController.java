package com.panha.springhomework.restcontroller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
public class FileRestController {

    @PostMapping("/upload")
    public String upLoadFile(@RequestParam("file") MultipartFile file){
        File uploadedFile = new File("C:\\Users\\panha long\\IdeaProjects\\fromTeacher\\New folder\\KPS-009-LONGPANHA-SPRING-HW\\src\\main\\resources\\image\\"+file.getOriginalFilename());

        try {
            uploadedFile.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(uploadedFile);
            fileOutputStream.write(file.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "localhost:8080/image/"+uploadedFile.getName();
    }

}
