package com.panha.springhomework.configuration;

import com.panha.springhomework.service.impl.UserServiceImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurtyConfiguration extends WebSecurityConfigurerAdapter {

    private UserServiceImplement userservice;
    private BCryptPasswordEncoder encoder;

    @Autowired
    public void setUserservice(UserServiceImplement userservice) {
        this.userservice = userservice;
    }

    @Autowired
    public void setEncoder(BCryptPasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/categories/**").hasAnyRole("USER","ADMIN")
                .antMatchers(HttpMethod.POST,"/categories").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/categories/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/categories/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET,"/books/**").hasAnyRole("USER","ADMIN")
                .antMatchers(HttpMethod.POST,"/books").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/books/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/books/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userservice).passwordEncoder(encoder);
    }
}
