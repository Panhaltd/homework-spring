package com.panha.springhomework.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class FileUploadConfiguration implements WebMvcConfigurer {
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/image/**").addResourceLocations("file:\\C:\\Users\\panha long\\IdeaProjects\\fromTeacher\\New folder\\KPS-009-LONGPANHA-SPRING-HW\\src\\main\\resources\\image\\");
        }
}
