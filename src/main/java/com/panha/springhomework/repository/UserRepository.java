package com.panha.springhomework.repository;

import com.panha.springhomework.repository.dto.RoleDto;
import com.panha.springhomework.repository.dto.UserDto;
import com.panha.springhomework.repository.selectprovider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    @SelectProvider(type = UserProvider.class, method = "selectUserByUsernameSql")
    @Results({
            @Result(column = "user_id", property = "userId"),
            @Result(column = "user_name", property = "username"),
            @Result(column = "id",
                    property = "roles",
                    many = @Many(select = "selectRolesByUserId"))
    })
    UserDto selectUserByUsername(String username);

    @Select("select r.id, r.name from roles r\n" +
            "inner join users_roles ur on r.id = ur.role_id\n" +
            "where ur.user_id = #{id}")
    List<RoleDto> selectRolesByUserId(int id);

}
