package com.panha.springhomework.repository.selectprovider;

import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String getCategoryTitleById(){
        return new SQL(){{
            SELECT("title");
            FROM("tb_categories");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String getCategoryById(){
        return new  SQL(){{
            SELECT("*");
            FROM("tb_categories");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String getAllBooks(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            ORDER_BY("id");
            LIMIT("#{pagination.limit}");
            OFFSET("#{pagination.offset}");
        }}.toString();
    }

    public String getBookById(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String postBook(){
        return new SQL(){{
            INSERT_INTO("tb_books");
            VALUES("title","#{title}");
            VALUES("author","#{author}");
            VALUES("description","#{description}");
            VALUES("thumbnail","#{thumbnail}");
            VALUES("category_id","#{category.id}");
        }}.toString();
    }

    public String updateBook(){
        return new SQL(){{
            UPDATE("tb_books");
            SET("title=#{bookDto.title}");
            SET("author=#{bookDto.author}");
            SET("description=#{bookDto.description}");
            SET("thumbnail=#{bookDto.thumbnail}");
            SET("category_id=#{bookDto.category.id}");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String deleteBookById(){
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String getBooksByCategoryId(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("category_id=#{id}");
        }}.toString();
    }

    public String getBookByTitle(){{
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("title LIKE '%'||#{filter.title}||'%'");
        }}.toString();
    }}

    public String getBookByCategoryIdAndTitle(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("category_id=#{id}");
            AND().
            WHERE("title LIKE '%'||#{title}||'%'");
        }}.toString();
    }
    public String countAllBook(){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_books");
        }}.toString();
    }

}
