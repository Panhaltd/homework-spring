package com.panha.springhomework.repository.selectprovider;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

    public String selectUserByUsernameSql() {
        return new SQL(){{
            SELECT("*");
            FROM("users");
            WHERE("user_name = #{username}");
        }}.toString();
    }

}
